﻿using UnityEngine;
using System.Collections;

public class Inicio : MonoBehaviour {

	public void Salir(bool PressExit){
		if (PressExit) {
			Application.Quit();
		}
	}

	public void ChangeScene (int sceneToChangeTo) {
		Application.LoadLevel (sceneToChangeTo);
	}
	public int TiempoRetardo = 500;
	public void ChangeSceneRetard (int sceneToChangeTo) {
		System.Threading.Thread.Sleep(TiempoRetardo);
		Application.LoadLevel (sceneToChangeTo);
	}
}
